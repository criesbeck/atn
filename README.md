The Augmented Transition Network parser from 
[Artificial Intelligence Programming 2nd ed]
(https://www.amazon.com/Artificial-Intelligence-Programming-Eugene-Charniak-ebook/dp/B00HZLXGG2)
by Charniak, Riesbeck. McDermott, and Meehan.

Updated to use packages and ASDF. 

Breaking change: The ATN function needs to be passed the initial
parsing agenda, e.g., 

```
(atn '(john saw the girl) '((parse s)))

(atn '(the block is on the table) '((parse np))
```

This is to avoid hardwiring grammatical categories like S and NP 
into the ATN code package.