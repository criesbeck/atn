;;; A simple ATN-style parser
;;;
;;; Based on code in Artificial Intelligence Programming,
;;; 2nd Ed., by Charniak, Riesbeck, McDermott and Meehan

;;; Change log
;;; ----------
;;; 12/07/2020 CKR: Added agenda parameter to avoid
;;;   wiring grammatical terms into ATN package
;;;   moved DEFPACKAGE into package.lisp file
;;; 01/15/2007 CKR: Add get-categories with error check,
;;;   added empty sentence check to CATEGORY ATN function,
;;;   replaced GET's with hashtables, added DO-ACTION

(in-package #:atn)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Globals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Internal

(defparameter *atn-functions* (make-hash-table))
(defparameter *categories* (make-hash-table))
(defparameter *networks* (make-hash-table))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; public

(defmacro defword (word &rest cats)
  `(progn (setf (gethash ',word *categories*)
            ',cats)
     ',word))

(defmacro defnet (name &rest net)
  `(progn (setf (gethash ',name *networks*)
            ',net)
     ',name))


(defmacro defatn-function (name vars &body body)
  `(progn (setf (gethash ',name *atn-functions*)
            #'(lambda ,vars ,@body))
     ',name))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Structures
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; internal

(defstruct (state (:print-function state-printer))
  sentence parse agenda saver)

(defun state-printer (state stream depth)
  (declare (ignore depth))
  (format stream "STATE ~S ~S"
    (state-sentence state)
    (car (state-agenda state))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; only ATN is public

(defun atn (sentence agenda)
  (let* ((choice-points '())
         (state (make-state
                 :sentence sentence
                 :parse '()
                 :agenda agenda
                 :saver #'(lambda (state)
                            (push state choice-points)))))
    (do ((state state (pop choice-points)))
        ((do-state state) (car (state-parse state)))
      (when (null choice-points)
        (return nil)))))


(defun add-actions (state actions)
  (setf (state-agenda state)
    (append actions (state-agenda state))))


(defun add-constituent (state constituent)
  (push constituent (state-parse state)))

(defun categoryp (word cat)
  (member cat (get-categories word)))

(defun collect-constituent (parse-tree name)
  (let ((tail (member name parse-tree)))
    (if (null tail)
        (error "~S not found in ~S" name parse-tree)
      (cons (nreverse
             (ldiff parse-tree (cdr tail)))
            (cdr tail)))))

(defun do-action (action state)
  (let ((atn-fn (get-atn-function (car action))))
    (cond ((null atn-fn)
           (error "Unknown ATN form ~S" action))
          ((apply atn-fn state (cdr action))
           (do-state state))
          (t nil))))

(defun do-state (state)
  (cond ((not (null (state-agenda state)))
         (do-action (pop (state-agenda state)) state))
        ((not (null (state-sentence state))) nil)
        (t t)))

(defun get-atn-function (name)
  (gethash name *atn-functions*))

(defun get-categories (word)
  (or (gethash word *categories*)
      (error "Unknown word: ~S" word)))

(defun get-net (name) (gethash name *networks*))

(defun save-alternative (state actions)
  (let ((new-state (copy-state state)))
    (add-actions new-state actions)
    (funcall (state-saver new-state) new-state)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ATN Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; all ATN functions are public

(defatn-function category (state cat)
  (let ((sentence (state-sentence state)))
    (cond ((null sentence) nil)
          ((categoryp (car sentence) cat)
           (add-constituent state
                            `(,cat ,(car sentence)))
           (pop (state-sentence state))
           t)
          (t nil))))

(defatn-function done (state name)
  (setf (state-parse state)
    (collect-constituent (state-parse state)
                         name))
  t)

(defatn-function either (state first-action &rest other-actions)
  (dolist (action other-actions)
    (save-alternative state (list action)))
  (add-actions state (list first-action))
  t)

(defatn-function optional (state action)
  (save-alternative state nil)
  (add-actions state (list action))
  t)

(defatn-function optional* (state action)
  (save-alternative state nil)
  (add-actions state `(,action (optional* ,action)))
  t)

(defatn-function parse (state net-name)
  (add-constituent state net-name)
  (add-actions state
               `(,@(get-net net-name) (done ,net-name)))
  t)

(defatn-function seq (state &rest actions)
  (add-actions state actions)
  t)

