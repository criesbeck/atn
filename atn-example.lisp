
(in-package #:atn-example)

#|
  A simple example lexicon and grammar demonstrating how to use
  the ATN parser from AI Programming 2nd Ed.

  Updated for modern Common Lisp, with packages and ASD file.

  Breaking change: the function ATN requires a second argument with a list of the initial
  parsing goals.

  Sample calls:
  (atn '(john saw the girl) '((parse s)))
  (atn '(the block is in the box) '((parse s)))
  (atn '(the block) '((parse np)))

  (run-examples)
|#
  
(defun run-examples ()
  (dolist (sent '((john saw the girl)
                  (the block is in the box)
                  (the girl in the hall saw the block on the table)))
    (format t "~&~%~S =>" sent)
    (pprint (atn sent '((parse s))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lexicon
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defword a det)
(defword block noun verb)
(defword box noun)
(defword boy noun)
(defword girl noun)
(defword did aux verb)
(defword hall noun)
(defword in prep)
(defword is to-be aux)
(defword john proper-noun)
(defword not neg)
(defword on prep)
(defword red adj)
(defword saw verb)
(defword table noun verb)
(defword the det)
(defword you pronoun)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ATN Networks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defnet s
    (either
     (seq (parse np)
          (parse vp))
     (seq (category to-be)
          (parse np)
          (parse pred))))

(defnet np
    (either
     (category proper-noun)
     (category pronoun)
     (seq (category det)
          (optional* (category adj))
          (category noun)
          (optional* (parse pp)))))

(defnet vp
    (either
     (seq (optional (seq (category aux)
                         (category neg)))
          (category verb)
          (parse nP))
     (seq (category to-be)
          (optional (category neg))
          (parse pred))))

(defnet pred
    (either
     (category adj)
     (parse pp)))

(defnet pp
    (category prep)
  (parse np))